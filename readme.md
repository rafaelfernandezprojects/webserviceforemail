# Web Service for emails #

> 1. I created this Web Service to send emails to citizens with a notification showing a streets list and an image with highlighted streets plowed.
> 2. The tricky here was to use the MemoryStream object (see the sendEmail() funtion) together with an image generated automatically to send that image embedded into the emails body without attaching it.
> 3. This Web Service was written in C# and it was used massively in the Snow Removal Admin app to manage scheduled snow removal operations. This WS deployed around 5.000 emails daily during the winter months in St. John's.
> 4. It is still being used internally, but you can see the Snow Removal App video [here][video]
> 5. Or you can see the public version [here][video2]

[video]: https://www.youtube.com/watch?v=ijlsuFSrmt4&feature=youtu.be
[video2]: http://map.stjohns.ca/snow