﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net.Mail;
using System.Configuration;
using System.Web.Script.Services;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using System.Net.Mime;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
//using Newtonsoft.Json;

namespace WebServiceForEmail
{
    /// <summary>
    /// Summary description for WebServiceEmail
    /// </summary>
    [WebService(Namespace = "http://lisapp01/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebServiceEmail : System.Web.Services.WebService
    {
        [WebMethod(Description = "Description: Test. No params. Return OK")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string test()
        {
            return "OK";
        }

        [WebMethod(Description = "Description: Send an email based on parms (string from, string msg, string date).<br>The web service will send an email without the map")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SendEmailwhenCancel(string from, string msg, string date)
        {
            String tmpDate = date.Replace("\"", ""); //Replacing for a simple quote
            DateTime myDate = DateTime.Parse(tmpDate);
            //Modify Dates according requeriments
            string[] myDates = myDate.ToLongDateString().Split(' ');

            switch (myDates[1]) {
                case "January":
                    myDates[1] = "Jan";
                    break;
                case "February":
                    myDates[1] = "Feb";
                    break;
                case "March":
                    myDates[1] = "Mar";
                    break;
                case "April":
                    myDates[1] = "Apr";
                    break;
                case "May":
                    myDates[1] = "May";
                    break;
                case "June":
                    myDates[1] = "Jun";
                    break;
                case "July":
                    myDates[1] = "Jul";
                    break;
                case "August":
                    myDates[1] = "Aug";
                    break;
                case "September":
                    myDates[1] = "Sep";
                    break;
                case "October":
                    myDates[1] = "Oct";
                    break;
                case "November":
                    myDates[1] = "Nov";
                    break;
                case "December":
                    myDates[1] = "Dec";
                    break;
            }
            myDates[2] = myDates[2].Replace(",", @"");
            string myFormatedDate = myDates[0] + " " + myDates[1] + ". " + myDates[2];

            MailMessage newEmail = new MailMessage(from, "DOWNTOWNSNOWREMOVAL@LISTSERV.STJOHNS.CA");
            //MailMessage newEmail = new MailMessage(from, "rfernandez@stjohns.ca");

            
            //Customer Service Centre Operators (CSR)
            newEmail.Bcc.Add(new MailAddress("citserv311@stjohns.ca", "Customer Service Centre Operators"));

            newEmail.Subject = "Downtown Snow Removal - " + myDate.ToLongDateString();
            newEmail.IsBodyHtml = true;

            //Generating Body
            string body = "<p style='font-family: Arial, Helvetica, sans-serif;font-size: 0.875em;'>" + msg + " <b>" + myFormatedDate + "</b><br><br>";
            body += "Operations were cancelled.</p>";
            body += "<p style='font-family: Arial, Helvetica, sans-serif;font-size: 0.875em;'>For updates and last notifications, please visit <i><a href='http://map.stjohns.ca/snow'>http://map.stjohns.ca/snow</a></i> </p>";
            body += "<h4 style='margin-bottom: -0.9375em;'>DISCLAIMER</h4><p style='font-family: Arial, Helvetica, sans-serif;font-size: 0.6875em;font-style: italic;'> To protect your " +
                "privacy the City of St. John's has strict controls in place. Your e-mail address will only be used to subscribe to this e-mail update and for no other purpose.<br>" +
                "Information contained in City of St. John's e-Updates is provided as a public service and solely for the user's information. Information is provided without warranty or guarantee of any kind, express or implied. The City of St. John's cannot guarantee that all information is current or accurate. Users should verify information before acting on it." + 
                "The City of St. John's will not be liable for any loss or damages of any nature, either direct or indirect, arising from the use of information provided in this e-Update.<br>" + 
                "If you have any questions about City of St. John's e-Updates contact accessstjohns@stjohns.ca.<br><br><hr></p>";

            AlternateView foot = AlternateView.CreateAlternateViewFromString(body, null, "text/html");

            newEmail.AlternateViews.Add(foot);

            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.Host = "mx.stjohns.ca";
            //client.Host = "barracuda.secure.city.st-johns.nf.ca";
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            try
            {
                client.Send(newEmail);
                return "OK";
            }
            catch (SmtpFailedRecipientsException ex)
            {
                return "OK";
            }
                /*
            catch (Exception ex)
            {
                if (ex is SmtpFailedRecipientsException)
                {

                    return "OK";
                }
                else
                {
                    Console.WriteLine("Exception caught in CreateTestMessage2(): {0}", ex.ToString());
                    return "Exception caught in send email: " + ex.ToString();
                }

                //throw;
            }     
                 * */
        }

        [WebMethod(Description = "Description: Send an email based on parms (string from, string msg, string date, string streets, string url).<br>attaching an inline image (the map) and passing the list of scheduled streets")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SendEmail(string from, string msg, string date, string streets, string url)
        {
            String tmpDate = date.Replace("\"", ""); //Replacing for a simple quote
            DateTime myDate = DateTime.Parse(tmpDate);

            //Modify Dates according requeriments
            string[] myDates = myDate.ToLongDateString().Split(' ');

            switch (myDates[1]) {
                case "January":
                    myDates[1] = "Jan";
                    break;
                case "February":
                    myDates[1] = "Feb";
                    break;
                case "March":
                    myDates[1] = "Mar";
                    break;
                case "April":
                    myDates[1] = "Apr";
                    break;
                case "May":
                    myDates[1] = "May";
                    break;
                case "June":
                    myDates[1] = "Jun";
                    break;
                case "July":
                    myDates[1] = "Jul";
                    break;
                case "August":
                    myDates[1] = "Aug";
                    break;
                case "September":
                    myDates[1] = "Sep";
                    break;
                case "October":
                    myDates[1] = "Oct";
                    break;
                case "November":
                    myDates[1] = "Nov";
                    break;
                case "December":
                    myDates[1] = "Dec";
                    break;
            }
            myDates[2] = myDates[2].Replace(",", @"");
            string myFormatedDate = myDates[0] + " " + myDates[1] + ". " + myDates[2];

            List<String> myStreets = new List<String>();
            List<String> myStreetsUpdated = new List<String>();
            myStreets = streets.Split(',').ToList();

            foreach (var item in myStreets)
            {
                String item2 = item.Replace("999", @"'"); //Replacing for a simple quote
                String item3 = Regex.Replace(item2, "[^a-z^()^ ^'^\\w-]+", string.Empty, RegexOptions.IgnoreCase);
                myStreetsUpdated.Add(item3);
            }
            
            var webClient = new WebClient();
            //byte[] image = webClient.DownloadData("http://www.google.com/images/logos/ps_logo2.png");
            byte[] image = webClient.DownloadData(url);
            
            MemoryStream logo = new MemoryStream(image);
            
            MailMessage newEmail = new MailMessage(from, "DOWNTOWNSNOWREMOVAL@LISTSERV.STJOHNS.CA");
            //newEmail.Bcc.Add(new MailAddress("rfernandez@stjohns.ca", "Rafael Fernandez"));
            

            newEmail.Subject = "Downtown Snow Removal - " + myDate.ToLongDateString();
            newEmail.IsBodyHtml = true;

            LinkedResource footerImg = new LinkedResource(logo, "image/jpeg");
            footerImg.ContentId = "companyLogo";

            //Generating Body
            string body = "<p style='font-family: Arial, Helvetica, sans-serif;font-size: 0.875em;'>" + msg +
                " <b>" + myFormatedDate + "</b>, as snow removal is in effect. To avoid being ticketed or towed, vehicles should be moved from the following " + myStreetsUpdated.Count().ToString();
            if (myStreetsUpdated.Count() > 1) body += " streets before midnight tonight.<br><br>";
            if (myStreetsUpdated.Count() == 1) body += " street before midnight tonight.<br><br>";
            int myStreetCount = 1;
            foreach (var street in myStreetsUpdated)
            {
                string newStreet = convertStreet(street);
                body += myStreetCount + ". " + newStreet + "<br>";
                myStreetCount++;
            }
            body = body.Remove(body.Length - 4); //Removing last <br>
            body += "</p><p> <img src=cid:companyLogo /> </p>";
            body += "<p style='font-family: Arial, Helvetica, sans-serif;font-size: 0.875em;'>For updates and last notifications, please visit <i><a href='http://map.stjohns.ca/snow'>http://map.stjohns.ca/snow</a></i> </p>";
            body += "<p style='font-family: Arial, Helvetica, sans-serif;font-size: 0.875em;'> Vehicles impeding the snow removal operation will be removed and impounded at the owner's expense. The towing and administration fee is $250, and the impounding fee is $25 each day the vehicle is held in storage. If proof of registration and insurance is not provided the vehicle can only be towed, not driven, from the impound lot.<hr> </p>";
            body += "<h4 style='margin-bottom: -0.9375em;'>DISCLAIMER</h4><p style='font-family: Arial, Helvetica, sans-serif;font-size: 0.6875em;font-style: italic;'> To protect your " +
                "privacy the City of St. John's has strict controls in place. Your e-mail address will only be used to subscribe to this e-mail update and for no other purpose.<br>" +
                "Information contained in City of St. John's e-Updates is provided as a public service and solely for the user's information. Information is provided without warranty or guarantee of any kind, express or implied. The City of St. John's cannot guarantee that all information is current or accurate. Users should verify information before acting on it." + 
                "The City of St. John's will not be liable for any loss or damages of any nature, either direct or indirect, arising from the use of information provided in this e-Update.<br>" + 
                "If you have any questions about City of St. John's e-Updates contact accessstjohns@stjohns.ca.<br><br><hr></p>";

            AlternateView foot = AlternateView.CreateAlternateViewFromString(body, null, "text/html");

            foot.LinkedResources.Add(footerImg);

            newEmail.AlternateViews.Add(foot);

            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.Host = "mx.stjohns.ca";
            //client.Host = "barracuda.secure.city.st-johns.nf.ca";
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            try
            {
                client.Send(newEmail);
                return "OK";
            }
            catch (SmtpFailedRecipientsException ex)
            {
                return "OK";
            }
            /*
            catch (Exception ex)
            {
                if (ex is SmtpFailedRecipientsException)
                {

                    return "OK";
                }
                else
                {
                    Console.WriteLine("Exception caught in CreateTestMessage2(): {0}", ex.ToString());
                    return "Exception caught in send email: " + ex.ToString();
                }

                //throw;
            }     
             * */
        }

        private string convertStreet(string street)
        {
            string firstPart = street.Substring(0, street.LastIndexOf('(')+1);
            int lngSecondPart = street.LastIndexOf('-') - street.LastIndexOf('(');
            string secondPart = street.Substring(street.LastIndexOf('(') + 1, lngSecondPart-1);
            int lngThirdPart = street.LastIndexOf(')') - street.LastIndexOf('-');
            string thirdPart = street.Substring(street.LastIndexOf('-') + 1, lngThirdPart);
            return firstPart + "from " + secondPart + " to " + thirdPart;
        }
    }
}
